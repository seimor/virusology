; nasm.exe -f win64 lab1_stack.s -o lab1_stack.obj
; GoLink.exe /console /entry _main lab1_stack.obj kernel32.dll

bits 64

global _main
extern ExitProcess, GetStdHandle, WriteConsoleA, \
        ReadConsoleA, GetConsoleMode, SetConsoleMode

STD_INPUT_HANDLE    equ -10
STD_OUTPUT_HANDLE   equ -11
ENABLE_LINE_INPUT   equ   2

STACK_SIZE          equ  16

section .text

_main:

hConsoleOut     equ 40
hConsoleIn      equ 48
lpMode          equ 56
chRead          equ 64
chCount         equ 72

    and rsp, 0fffffffffffffff0h

    sub rsp, 80
    mov rcx, STD_OUTPUT_HANDLE
    call GetStdHandle
    mov [rsp+hConsoleOut], rax

    mov rcx, STD_INPUT_HANDLE
    call GetStdHandle
    mov [rsp+hConsoleIn], rax

    mov rcx, qword [rsp+hConsoleIn]
    lea rdx, [rsp+lpMode]
    call GetConsoleMode

    mov rcx, qword [rsp+hConsoleIn]
    mov rdx, qword [rsp+lpMode]
    mov rbx, ENABLE_LINE_INPUT
    not rbx
    and rdx, rbx
    call SetConsoleMode

    call init_my_stack

read:
    mov rcx, qword [rsp+hConsoleIn]
    lea rdx, [rsp+chRead]
    mov r8, 1
    lea r9, [rsp+chCount]
    mov qword [rsp+32], 0
    call ReadConsoleA

    xor rcx, rcx
    mov cl, [rsp+chRead]
    call my_push

    mov rcx, qword [rsp+hConsoleOut]
    lea rdx, [rsp+chRead]
    mov r8, 1
    lea r9, [rsp+chCount]
    mov qword [rsp+32], 0
    call WriteConsoleA

    cmp byte [rsp+chRead], 0Dh
    jne read

write_new_line:
    mov word [rsp+chRead], 0D0Ah
    mov rcx, qword [rsp+hConsoleOut]
    lea rdx, [rsp+chRead]
    mov r8, 2
    lea r9, [rsp+chCount]
    mov qword [rsp+32], 0
    call WriteConsoleA

write:
    call my_pop
    mov qword [rsp+chRead], rax

    mov rcx, qword [rsp+hConsoleOut]
    lea rdx, [rsp+chRead]
    mov r8, 1
    lea r9, [rsp+chCount]
    mov qword [rsp+32], 0
    call WriteConsoleA

    mov rbx, qword [my_stack_pointer]
    cmp rbx, STACK_SIZE
    jl write

_exit:
    mov rcx, qword [rsp+hConsoleOut]
    lea rdx, [test_msg]
    mov r8, 0Dh
    lea r9, [rsp+chCount]
    mov qword [rsp+32], 0
    call WriteConsoleA

    mov rcx, qword [rsp+hConsoleIn]
    mov edx, dword [rsp+lpMode]
    call SetConsoleMode

    add rsp, 80

    xor rcx, rcx
    call ExitProcess

init_my_stack:
    mov qword [my_stack_pointer], STACK_SIZE
    ret

my_push:
    cmp qword [my_stack_pointer], 0
    je __exit
    push rbx
    dec qword [my_stack_pointer]
    mov rbx, qword [my_stack_pointer]
    mov byte [my_stack+rbx], cl
    pop rbx
    ret

my_pop:
    push rbx
    xor rax, rax
    mov rbx, qword [my_stack_pointer]
    mov al, byte [my_stack+rbx]
    inc qword [my_stack_pointer]
    pop rbx
    ret

__exit:
    add rsp, 8
    jmp write_new_line


section .stack data
    test_msg                    db 10, "Am I alive?", 10, 0
    my_stack   times STACK_SIZE db 0
    my_stack_pointer            dq 0